library(plyr)

grid <- with(training, seq(min(age), max(age), length = 100))
normaldens <- ddply(training, "salary", function(df) {
  data.frame( 
    predicted = grid,
    density = dnorm(grid, mean(df$age), sd(df$age))
  )
})
  
p <- ggplot(data = training,aes(x = age)) + 
  facet_grid(salary~.) + 
  geom_histogram(aes(y = ..density..), binwidth=1) + geom_line(data = normaldens, aes(x = predicted, y = density), colour = "red")

print(p)
