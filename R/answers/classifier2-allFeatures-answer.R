# Naive Bayes classifier

trim <- function( x ) {
  gsub("(^[[:space:]]+|[[:space:]]+$)", "", x)
}


# Data tables that are needed
salary.table <- table(training[["salary"]])
education.table <- table(training[["education"]], training[["salary"]])
workclass.table <- table(training[["workclass"]], training[["salary"]])
education.num.table <- table(training[["education.num"]], training[["salary"]])
marital.status.table <- table(training[["marital.status"]], training[["salary"]])
occupation.table <- table(training[["occupation"]], training[["salary"]])
relationship.table <- table(training[["relationship"]], training[["salary"]])
race.table <- table(training[["race"]], training[["salary"]])
sex.table <- table(training[["sex"]], training[["salary"]])
native.country.table <- table(training[["native.country"]], training[["salary"]])


# Factors of "<=50K" and ">50K" to make code easier
lt50K <- factor("<=50K", levels(training[,"salary"]))
gt50K <- factor(">50K",  levels(training[,"salary"]))


# Classifier function
#  input x: vector of feature values
#  input printScores: if TRUE then prints the score for the vector
#  output : classification of vector into a class

classifier2 <- function(x, printScores=FALSE) {
  
  # Start with the a-priori probabilities
  # p(<=50K)
  lowSalaryScore <- salary.table[lt50K]/sum(salary.table)
  # p(>50K)
  highSalaryScore <- salary.table[gt50K]/sum(salary.table)
  
  # Education
  edu <- x[["education"]]
  if (!is.na(edu)) {
    # p(data|class)  
    lowSalaryScore  = lowSalaryScore  * education.table[edu,lt50K]/sum(education.table[,lt50K])
    highSalaryScore = highSalaryScore * education.table[edu,gt50K]/sum(education.table[,gt50K])
  }

  # Workclass
  wc <- x[["workclass"]]
  if (!is.na(wc)) {
    # p(data|class)  
    lowSalaryScore  <- lowSalaryScore  * workclass.table[wc,lt50K]/sum(workclass.table[,lt50K])
    highSalaryScore <- highSalaryScore * workclass.table[wc,gt50K]/sum(workclass.table[,gt50K])
  }
  
  # education number
  f <- trim(x[["education.num"]])
  if (!is.na(f)) {
    # p(data|class)  
    table <- education.num.table
    lowSalaryScore  <- lowSalaryScore  * table[f,lt50K]/sum(table[,lt50K])
    highSalaryScore <- highSalaryScore * table[f,gt50K]/sum(table[,gt50K])
  }
  
  # marital status
  f <- x[["marital.status"]]
  if (!is.na(f)) {
    # p(data|class)  
    table <- marital.status.table
    lowSalaryScore  <- lowSalaryScore  * table[f,lt50K]/sum(table[,lt50K])
    highSalaryScore <- highSalaryScore * table[f,gt50K]/sum(table[,gt50K])
  }
  
  # occupation
  f <- x[["occupation"]]
  if (!is.na(f)) {
    # p(data|class)  
    table <- occupation.table
    lowSalaryScore  <- lowSalaryScore  * table[f,lt50K]/sum(table[,lt50K])
    highSalaryScore <- highSalaryScore * table[f,gt50K]/sum(table[,gt50K])
  }
  
  # relationship
  f <- x[["relationship"]]
  if (!is.na(f)) {
    # p(data|class)  
    table <- relationship.table
    lowSalaryScore  <- lowSalaryScore  * table[f,lt50K]/sum(table[,lt50K])
    highSalaryScore <- highSalaryScore * table[f,gt50K]/sum(table[,gt50K])
  }
  
  # sex
  f <- x[["sex"]]
  if (!is.na(f)) {
    # p(data|class)  
    table <- sex.table
    lowSalaryScore  <- lowSalaryScore  * table[f,lt50K]/sum(table[,lt50K])
    highSalaryScore <- highSalaryScore * table[f,gt50K]/sum(table[,gt50K])
  }
  
  # race
  f <- x[["race"]]
  if (!is.na(f)) {
    # p(data|class)  
    table <- race.table
    lowSalaryScore  <- lowSalaryScore  * table[f,lt50K]/sum(table[,lt50K])
    highSalaryScore <- highSalaryScore * table[f,gt50K]/sum(table[,gt50K])
  }
  
  # sex
  f <- x[["native.country"]]
  if (!is.na(f)) {
    # p(data|class)  
    table <- native.country.table
    lowSalaryScore  <- lowSalaryScore  * table[f,lt50K]/sum(table[,lt50K])
    highSalaryScore <- highSalaryScore * table[f,gt50K]/sum(table[,gt50K])
  }

  # Age
  age <- as.numeric(x[["age"]])
  if (!is.na(age)) {
    lowSalaryScore  <- lowSalaryScore  * dnorm(age, mean=36.78374 , sd=14.02009)
    highSalaryScore <- highSalaryScore * dnorm(age, mean=44.24984 , sd=10.51903)
  }
  
  if (printScores) {
    print(paste0("<=50K: ", lowSalaryScore, " >50K: ", highSalaryScore))
  }
  
  highSalaryScore/lowSalaryScore
}
